# Interview Exercise

Implement a server that responds to quote requests.

## Requirements

The server should accept TCP connections from one or more clients and respond to their requests.  Requests are sent on a 
single line, in the format of:

    {security ID} (BUY|SELL) {quantity}

Where `security ID` and `quantity` are integers.

For example:

    123 BUY 100

Is a request to buy 100 shares of security 123.

This should be responded to with a single line with a single numeric value representing the quoted price.

To calculate the quote price, two interfaces have been provided.

* `QuoteCalculationEngine` - to calculate the quote price based on a security, buy/sell indicator, requested
quantity and reference price.
* `ReferencePriceSource` - source of reference prices for the `QuoteCalculationEngine`.

The server should be capable of handling a large number of quote requests and be able to respond in a timely manner.

## Assumptions

This specification is intentionally vague, and numerous assumptions need to be made.  These assumptions should be 
documented and justified.

## Testing

Evidence should be provided that the server works correctly.


## Console 
* run MyServer <port> <no_threads> 
(default: port=3333, no_threads=2)

Beware: hard code a set of security id
final int[] secIds = new int[]{1234, 3339, 6667, 8899};

1. read service: reading input from clients
2. pricing service: pricing task of quote request
3. write service: reply price in order of incoming request
4. market data stub: mock the market data provider

> Task :MyServer.main()
Waiting for client on port 3333...
Consumer started
Producer started
Pricer started
Just connected to /127.0.0.1:60853
clinet id=0 is opened
Waiting for client on port 3333...


* run MyClient <port> <no_threads> 
(default: port=3333)
> Task :MyClient.main()
input:
1234 BUY 100
client sent: 1234 BUY 100
waiting reply....
Server reply: 41.5
input:





