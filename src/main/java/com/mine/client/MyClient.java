package com.mine.client;

import java.net.*;
import java.io.*;
class MyClient{
    /**
     *
     * @param args port (default:3333)
     * @throws Exception
     */
    public static void main(String args[])throws Exception{
        int port = args.length > 0 && args[0]!=null ? Integer.parseInt(args[0]) : 3333;
        Socket s=new Socket("localhost",port);
        DataInputStream din=new DataInputStream(s.getInputStream());
        DataOutputStream dout=new DataOutputStream(s.getOutputStream());
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

        String str="",str2="";
        while(!str.equals("stop")){
            System.out.println("input:");
            str=br.readLine();
            dout.writeUTF(str);
            dout.flush();
            System.out.println("client sent: " + str);
            System.out.println("waiting reply....");
            str2=din.readUTF();
            System.out.println("Server reply: "+str2);
        }

        dout.close();
        s.close();
    }}
