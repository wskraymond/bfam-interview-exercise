package com.mine.server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Optional;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

class MyServer{
    public static class ClientTask {
        private final int id;
        private final Socket socket;
        private final DataInputStream in;
        private final DataOutputStream out;
        private final AtomicLong inSequence = new AtomicLong(0L);
        private final AtomicLong nextOutSeq = new AtomicLong(0L);
        private final BlockingQueue<QuoteRequest> inQ;
        private final BlockingQueue<QuoteReply> outQ;
        private volatile boolean open;

        public ClientTask(int id, Socket socket, int qlimit) throws IOException {
            this.id = id;
            this.socket = socket;
            this.in = new DataInputStream(this.socket.getInputStream());
            this.out = new DataOutputStream(this.socket.getOutputStream());
            this.inQ = new LinkedBlockingQueue(qlimit);
            this.outQ = new PriorityBlockingQueue<>(qlimit);
            this.open = true;

            System.out.println(String.format("client id=%d is opened",id));
        }

        /**
         *
         * @return
         * @throws IOException
         * @throws InterruptedException
         */
        public Optional<QuoteRequest> read(int timeoutMS) throws IOException, InterruptedException {
            if(in.available() == 0) {
                Thread.sleep(timeoutMS);
            }

            if(in.available() > 0) {
                String msg = in.readUTF();
                String[] fields = msg.split(" ");

                long seqNo = inSequence.getAndIncrement();
                int securityId = Integer.parseInt(fields[0]);
                boolean buy = "BUY".equals(fields[1]) ? true : false;
                int quantity = Integer.parseInt(fields[2]);
                return Optional.of(new QuoteRequest(seqNo, securityId, buy, quantity));
            }

            return Optional.empty();
        }

        public void write(QuoteReply quoteReply) throws IOException {
            out.writeUTF(String.valueOf(quoteReply.getPrice()));
            out.flush();
            nextOutSeq.compareAndSet(nextOutSeq.get(), quoteReply.getSeqNo() + 1);
        }

        public boolean isOpen(){
            return open;
        }

        public void close() throws IOException {
            open = false;
            in.close();
            out.close();
            socket.close();
            System.out.println(String.format("client id=%d is closed",id));
        }

        public BlockingQueue<QuoteRequest> getInQ() {
            return inQ;
        }

        public BlockingQueue<QuoteReply> getOutQ() {
            return outQ;
        }

        public long getNextOutSeq(){
            return this.nextOutSeq.get();
        }
    }

    private final ExecutorService readService;
    private final ExecutorService writeService;
    private final ServerSocket serverSocket;
    private final PricingEngine pricingEngine;
    private final AtomicInteger idSequence = new AtomicInteger(0);

    private final BlockingQueue<ClientTask> writeRoundRobinQ;
    private final BlockingQueue<ClientTask> readRoundRobinQ;
    private volatile boolean open;

    public MyServer(int port,
                    PricingEngine pricingEngine,
                    ExecutorService readService,
                    ExecutorService writeService
                    ) throws IOException{
        this.serverSocket = new ServerSocket(port);
        this.pricingEngine = pricingEngine;

        this.writeRoundRobinQ = new LinkedBlockingQueue<>();
        this.readRoundRobinQ = new LinkedBlockingQueue<>();

        this.readService = readService;
        this.writeService = writeService;
        this.open = true;
    }

    public void accept(){
            try {
                while(open) {
                    System.out.println("Waiting for client on port " +
                            serverSocket.getLocalPort() + "...");
                    Socket socket = serverSocket.accept();
                    System.out.println("Just connected to " + socket.getRemoteSocketAddress());

                    final ClientTask clientTask = new ClientTask(idSequence.getAndIncrement(), socket, 100000);
                    pricingEngine.register(clientTask);
                    writeRoundRobinQ.put(clientTask);
                    readRoundRobinQ.put(clientTask);
                }
            } catch (SocketTimeoutException s) {
                System.out.println("Socket timed out!");
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            } finally {
                this.close();
            }
    }

    public void consume(){
        System.out.println("Consumer started");
        try {
            while(open){
                final MyServer.ClientTask clientTask = readRoundRobinQ.take();
                if(clientTask.isOpen()) {
                    readService.execute(() -> {
                        try {
                            Optional<QuoteRequest> qr = clientTask.read(100);
                            if(qr.isPresent()) {
                                clientTask.getInQ().put(qr.get());
                            }

                            if(clientTask.isOpen() ) {
                                readRoundRobinQ.put(clientTask);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (clientTask.isOpen()) {
                                try {
                                    clientTask.close();
                                    System.out.println("client closed");
                                } catch (IOException ex) {
                                    ex.printStackTrace();
                                }
                            }
                        }
                    });
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Consumer end");
    }

    public void produce(){
        System.out.println("Producer started");
        try {
            while(open){
                final MyServer.ClientTask clientTask = writeRoundRobinQ.take();
                if(clientTask.isOpen()) {
                    writeService.execute(() -> {
                        try {
                            final QuoteReply qr = clientTask.getOutQ().poll(1000, TimeUnit.MILLISECONDS);
                            if(qr!=null ){
                                if( qr.getSeqNo()==clientTask.getNextOutSeq()) {
                                    clientTask.getOutQ().remove(qr);
                                    clientTask.write(qr);
                                } else {
                                    clientTask.getOutQ().put(qr);
                                }
                            }

                            if(clientTask.isOpen() ) {
                                writeRoundRobinQ.put(clientTask);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (clientTask.isOpen()) {
                                try {
                                    clientTask.close();
                                    System.out.println("client closed");
                                } catch (IOException ex) {
                                    ex.printStackTrace();
                                }
                            }
                        }
                    });
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Producer end");
    }

    public void closeClient(ClientTask clientTask) throws IOException {
        clientTask.close();
    }

    public void close() {
        if(open) {
            try {
                open = false;
                //FIXME: close all client
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 1. read service: reading input from clients
     * 2. pricing service: task of quote request
     * 3. jwrite service: reply price in sequence
     * 4. market data stub: mock the market data provider
     *
     * @param args port (default:3333) , no_threads(for each service)
     * @throws Exception
     */
    public static void main(String args[])throws Exception{
        final int[] secIds = new int[]{1234, 3339, 6667, 8899};
        int port = args.length > 0 && args[0]!=null ? Integer.parseInt(args[0]) : 3333;
        int no_threads = args.length > 1 &&  args[1]!=null ? Integer.parseInt(args[1]) : 2;


        //fairness: round robin to write/read/price ......
        ExecutorService priceService = Executors.newFixedThreadPool(no_threads);
        ExecutorService writeService = Executors.newFixedThreadPool(no_threads);
        ExecutorService readService = Executors.newFixedThreadPool(no_threads);

        MarketDataSrc src = new MarketDataSrc() {
            @Override
            public double get(int securityId) {
                int min = 50;
                int max = 100;
                return (double)Math.floor(Math.random()*(max-min+1)+min);
            }
        };

        //cache market data
        MarketDataProvider marketDataProvider = new MarketDataProvider(src);
        src.subscribe(marketDataProvider);

        //stub: linear equation for pricing
        PricingEngine pricingEngine = new PricingEngine(priceService,
                (securityId, referencePrice, buy, quantity)->referencePrice/2 + (buy ? -0.5 : 0.5 ), marketDataProvider);

        MyServer server = new MyServer(port,
                pricingEngine,
                readService,
                writeService);

        CompletableFuture dataStub = CompletableFuture.runAsync(()->{
            try {
                while(server.open){
                    int min = 0;
                    int max = secIds.length-1;

                    int index = (int)Math.floor(Math.random()*(max-min+1)+min);
                    src.update(secIds[index], src.get(secIds[index]));
                    Thread.sleep(3000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });


        try {
            CompletableFuture s = CompletableFuture.runAsync(server::accept);

            CompletableFuture c = CompletableFuture.runAsync(server::consume);

            CompletableFuture p = CompletableFuture.runAsync(server::produce);

            CompletableFuture pricing = CompletableFuture.runAsync(pricingEngine::price);

            CompletableFuture end = CompletableFuture.anyOf(dataStub, s, c, p, pricing).whenCompleteAsync((result, throwable) -> {
                server.close();
                if(throwable!=null){
                    System.out.println("server close - " + throwable.getMessage());
                }
            });

            end.get();
        } finally {
            server.close();
            System.out.println("Server End!!!!!!");
        }
    }
}
