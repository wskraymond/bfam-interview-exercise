package com.mine.server;

import com.example.marketmaker.ReferencePriceSource;
import com.example.marketmaker.ReferencePriceSourceListener;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public abstract class MarketDataSrc implements ReferencePriceSource {
    private final List<ReferencePriceSourceListener> observers = new CopyOnWriteArrayList();

    @Override
    public void subscribe(ReferencePriceSourceListener listener) {
        observers.add(listener);
    }

    public void update(int securityId, double price){
        for(ReferencePriceSourceListener listener: observers){
            listener.referencePriceChanged(securityId, price);
        }
    }

    @Override
    public abstract double get(int securityId);
}
