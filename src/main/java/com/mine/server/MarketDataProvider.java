package com.mine.server;

import com.example.marketmaker.ReferencePriceSource;
import com.example.marketmaker.ReferencePriceSourceListener;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

public class MarketDataProvider implements ReferencePriceSourceListener {
    private final Map<Integer, Double> refBySecId = new ConcurrentHashMap<>(); //FIXME: LRU should be used...
    private final MarketDataSrc marketDataSrc;

    public MarketDataProvider(MarketDataSrc marketDataSrc) {
        this.marketDataSrc = marketDataSrc;
    }

    @Override
    public void referencePriceChanged(int securityId, double price) {
        refBySecId.put(securityId, price);
    }

    public double get(int securityId){
        if(!refBySecId.containsKey(securityId)){
            refBySecId.put(securityId,marketDataSrc.get(securityId));
        }

        return refBySecId.get(securityId);
    }
}
