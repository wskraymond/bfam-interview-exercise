package com.mine.server;

import java.util.concurrent.CompletableFuture;

public class QuoteReply implements Comparable<QuoteReply> {
    private long seqNo;
    private double price;

    public QuoteReply(long seqNo, double price) {
        this.seqNo = seqNo;
        this.price = price;
    }

    public long getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(long seqNo) {
        this.seqNo = seqNo;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public int compareTo(QuoteReply o) {
        return Long.compare(seqNo, o.getSeqNo());
    }
}
