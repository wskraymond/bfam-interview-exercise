package com.mine.server;

public class QuoteRequest {
    private long seqNo;
    private int securityId;
    private boolean buy;
    private int quantity;


    public QuoteRequest(long seqNo, int securityId, boolean buy, int quantity) {
        this.seqNo = seqNo;
        this.securityId = securityId;
        this.buy = buy;
        this.quantity = quantity;
    }

    public long getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(long seqNo) {
        this.seqNo = seqNo;
    }

    public int getSecurityId() {
        return securityId;
    }

    public void setSecurityId(int securityId) {
        this.securityId = securityId;
    }

    public boolean isBuy() {
        return buy;
    }

    public void setBuy(boolean buy) {
        this.buy = buy;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
