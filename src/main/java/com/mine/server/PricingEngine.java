package com.mine.server;

import com.example.marketmaker.QuoteCalculationEngine;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;

public class PricingEngine{
    private final ExecutorService service;
    private final QuoteCalculationEngine calculator;
    private final MarketDataProvider marketDataProvider;
    private final BlockingQueue<MyServer.ClientTask> roundRobinQ;
    private volatile boolean open;

    public PricingEngine(ExecutorService service,
                         QuoteCalculationEngine calculator,
                         MarketDataProvider marketDataProvider) {
        this.service = service;
        this.calculator = calculator;
        this.marketDataProvider = marketDataProvider;
        this.roundRobinQ = new LinkedBlockingQueue<>();
        this.open = true;
    }

    public void register(MyServer.ClientTask clientTask) throws InterruptedException {
        roundRobinQ.put(clientTask);
    }

    public boolean unregister(MyServer.ClientTask clientTask){
        return roundRobinQ.remove(clientTask);
    }

    public void close(){
        open = false;
    }

    public void price() {
        System.out.println("Pricer started");
        try {
            while(open){
                final MyServer.ClientTask clientTask = roundRobinQ.take();
                final QuoteRequest quoteRequest = clientTask.getInQ().poll();
                if(quoteRequest!=null) {
                    service.execute(() -> {
                        try {
                            double ref = marketDataProvider.get(quoteRequest.getSecurityId());
                            double price = calculator.calculateQuotePrice(quoteRequest.getSecurityId(), ref, quoteRequest.isBuy(), quoteRequest.getQuantity());
                            clientTask.getOutQ().put(new QuoteReply(quoteRequest.getSeqNo(), price));
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    });
                }

                if(clientTask.isOpen() ) {
                    roundRobinQ.put(clientTask);
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Pricer end");
    }

}
